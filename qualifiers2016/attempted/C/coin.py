import math
def isprime(inputnum):
    endnum=math.sqrt(inputnum)
    endint=int(endnum)
    #print endint
    flag=0
    for i in xrange(3,endint+1,2):
        if inputnum % i==0:
            return i,1
    if flag==0:
        return -1,0

list=[]
val=0
# raw_input() reads a string with a line of input, stripping the '\n' (newline) at the end.
# This is all you need for most Google Code Jam problems.
t = int(raw_input())  # read a line with a single integer
for i in range(0, t):
    n, j = [int(s) for s in raw_input().split(" ")]
#print n,j
#input collected now need to process
#we will test only the numbers in this range
baseStart=pow(2,n-1)+1
baseEnd=pow(2,n)-1
numbersNeeded=j
print "Case #1:"
f = open("outputlarge.txt", "a")
f.write("Case #1:"+"\n")
f.close()
#print baseStart,baseEnd
for i in xrange(baseStart,baseEnd+1,2):
    #print i
    if numbersNeeded<1:
        break
    list=[]
    #print i
    div,result=isprime(i)
    #print div,result
    if result==1:
        binary=bin(i)
        list.append(div)
        #now check for each of the bases from 3 to 10 if its is prime or not
        # if all of them say its not prime then our number is jamcoin
        lenbinary=len(binary)
        binarysliced=binary[2:lenbinary]
        lenbinary=len(binarysliced)
        #print binarysliced
        baseSatisfied=0
        for base in xrange(3,11):
            val=0
            counting = 0
            for rev in range(lenbinary-1,-1,-1):
                if binarysliced[rev] == "1":
                    k=pow(base, counting)
                    #print k
                    val+=k
                #print counting
                counting += 1
            counting-=1
            #print val
            #we got the respective base value now need to check if the value if prime or not
            # and continue only if the value is not prime for the given base aswell
            div,result=isprime(val)
            if result==1:
                list.append(div)
                baseSatisfied+=1
                #print baseSatisfied
                continue
            else:
                break
        if baseSatisfied==8:
            #we have got a jamcoin print the
            #  value
            f=open("outputlarge.txt","a")
            f.write(binarysliced+" ")



            print binarysliced,
            for m in range(0,9):
                f.write(str(list[m])+" ",)

                print list[m],
            print
            f.write("\n")
            f.close()
            numbersNeeded-=1